(defgroup todo-comment nil
  "Utility to add todo comments"
  :group 'tools)

(defcustom todo-comment-types '((?t "TODO") (?n "NOTE") (?i "INFO") (?f "FIXME") (?h "HACK"))
  "Possible types of the inserted comments"
  :group 'todo-comment)
(defcustom todo-comment-author ""
  "Default inserted author"
  :group 'todo-comment)
(defun insert-todo-comment ()
  (interactive)
  (if (eq todo-comment-author "")
      (customize-save-variable 'todo-comment-author (read-string "Author: ")))
  (let ((type (first (last (read-multiple-choice "Type: " todo-comment-types)))))
    (insert
     (format "%s%s - %s - %s - "
	     comment-start
	     type
	     todo-comment-author
	     (format-time-string "%Y-%m-%d" (current-time))
	     )
     )))
(provide 'todo-comment)
