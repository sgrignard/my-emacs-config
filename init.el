;;; init.el --- configuration
;;; Commentary:
;;; Code:
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; `use-package' to more easily install external packages
(require 'use-package)
(when init-file-debug
  (setq use-package-verbose t
        use-package-expand-minimally nil
        use-package-compute-statistics t
        debug-on-error t))

;; COMMON CONFIGURATION
(load-library "iso-transl")

; directory for manually installed packages
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; display column number
(setq column-number-mode t)
;; and line numbers in editor
(add-hook 'after-init-hook #'global-display-line-numbers-mode)
;; save opened buffers when exiting
(desktop-save-mode 1)
(setq desktop-restore-eager 25)
;; show matching parenthesis/block
(show-paren-mode 1)
; hide menu and toolbar
(menu-bar-mode -1)
(tool-bar-mode -1)
; move between window with M-(up/down/left/right)
(windmove-default-keybindings 'meta)
; set cursor type to box
(set-default 'cursor-type 'box)
; natively compile lisp by default
(setq native-comp-deferred-compilation t)
; default method for tramp: quicker than scp
(setq tramp-default-method "ssh")
; default to native compilation :-)
; only have y/n prompts
(fset 'yes-or-no-p 'y-or-n-p)
; do not pause if input during redisplay
(setq redisplay-dont-pause t)
; optimization for very long lines (minified code…)
(global-so-long-mode 1)
 ;; ediff tuning: use the current frame and split for larger screens
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)
;; Simpler binding for killing a buffer
(global-set-key (kbd "C-q") 'kill-current-buffer)


;; Tree-Sitter configuration
(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
(dolist (grammar treesit-language-source-alist)
	(unless (treesit-language-available-p (car grammar))
	  (treesit-install-language-grammar (car grammar))))
(use-package treesit-auto
 :ensure t
 :config
 (global-treesit-auto-mode))

(use-package ts-fold
  :requires s
  :load-path "~/.emacs.d/lisp/ts-fold"
  :hook prog-mode)

;; `M-x combobulate' (default: `C-c o o') to start using Combobulate
;; Do not forget to customize Combobulate to your liking:
;;
;;  M-x customize-group RET combobulate RET
;;
(use-package combobulate
  :preface
  ;; You can customize Combobulate's key prefix here.
  ;; Note that you may have to restart Emacs for this to take effect!
  (setq combobulate-key-prefix "C-c o")

  ;; Optional, but recommended.
  ;;
  ;; You can manually enable Combobulate with `M-x
  ;; combobulate-mode'.
  :hook ((python-ts-mode . combobulate-mode)
         (js-ts-mode . combobulate-mode)
         (css-ts-mode . combobulate-mode)
         (yaml-ts-mode . combobulate-mode)
         (json-ts-mode . combobulate-mode)
         (typescript-ts-mode . combobulate-mode)
         (tsx-ts-mode . combobulate-mode))
  ;; Amend this to the directory where you keep Combobulate's source
  ;; code.
  :load-path ("~/.emacs.d/combobulate"))

; vterm: high-performance shell implementation in emacs
; install libvterm-dev before if possible
(use-package vterm
  :ensure t
  :bind ("C-c v o" . vterm-other-window))

;; Fuzzy matching for most commands
(use-package ivy
  :ensure t
  :config
  (ivy-mode)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (setq ivy-use-selectable-prompt t)
  (setq ivy-count-format "(%d/%d) ")

  ;; enable this if you want `swiper' to use it
  ;; (setq search-default-mode #'char-fold-to-regexp)
  (setq ivy-re-builders-alist
	'((swiper . ivy--regex-plus)
          (t      . ivy--regex-fuzzy)))
  (global-set-key "\C-s" 'swiper)
  (global-set-key (kbd "C-c C-r") 'ivy-resume)
  (global-set-key (kbd "<f6>") 'ivy-resume))
(use-package counsel
  :ensure t
  :config
  (counsel-mode))
(use-package flx
  :ensure t
  :config
  (setq gc-cons-threshold 20000000))

;; Two callable functions for enabling/disabling tabs in Emacs
(defun disable-tabs () (setq indent-tabs-mode nil))
(defun enable-tabs  ()
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq indent-tabs-mode t)
  (setq tab-width custom-tab-width))

; crux: some extensions for emacs
(use-package crux :ensure t)

(use-package mood-line :ensure t)
(mood-line-mode)

(use-package shift-number
  :ensure t
  :bind (
	 ("C-c +" . shift-number-up)
	 ("C-c -" . shift-number-down)))

; company: completion at point
(use-package company
  :ensure t
  :config (add-hook 'after-init-hook 'global-company-mode))

; mu4e: mail reader
(require 'mu4e)
(setq mu4e-sent-folder "/Sent")
(setq mu4e-drafts-folder "/Drafts")
(setq mu4e-trash-folder "/Trash")

(require 'cmake-mode)
(require 'project-cmake)

; fira code with ligatures
(use-package fira-code-mode
  :ensure t
  :custom (fira-code-mode-disabled-ligatures '("[]" "#{" "#(" "#_" "#_(" "x")) ;; List of ligatures to turn off
  :hook prog-mode) ;; Enables fira-code-mode automatically for programming major modes

;; LSP servers in emacs
(add-hook 'prog-mode-hook 'eglot-ensure)

;; checkers for flymake
(use-package flymake-collection
  :ensure t
  :config (push
	   '(python-mode
	     (flymake-collection-pycodestyle :disabled t)) ; Never added.
	     flymake-collection-config)
  :hook (after-init . flymake-collection-hook-setup))

;; ANSI colors in eshell
(use-package xterm-color :ensure t)

(use-package visual-regexp :ensure t)

;; rg: use ripgrep from emacs
(use-package rg
  :ensure t
  :config(rg-enable-menu))

;; magit: git interaction
(use-package magit :ensure t)
(global-set-key (kbd "C-c g") 'magit-status)
(use-package magit-todos
  :ensure t
  :config (magit-todos-mode))
(use-package forge :ensure t)
(defclass forge-gitlab-http-repository (forge-gitlab-repository)
  ((issues-url-format         :initform "http://%h/%o/%n/issues")
   (issue-url-format          :initform "http://%h/%o/%n/issues/%i")
   (issue-post-url-format     :initform "http://%h/%o/%n/issues/%i#note_%I")
   (pullreqs-url-format       :initform "http://%h/%o/%n/merge_requests")
   (pullreq-url-format        :initform "http://%h/%o/%n/merge_requests/%i")
   (pullreq-post-url-format   :initform "http://%h/%o/%n/merge_requests/%i#note_%I")
   (commit-url-format         :initform "http://%h/%o/%n/commit/%r")
   (branch-url-format         :initform "http://%h/%o/%n/commits/%r")
   (remote-url-format         :initform "http://%h/%o/%n")
   (create-issue-url-format   :initform "http://%h/%o/%n/issues/new")
   (create-pullreq-url-format :initform "http://%h/%o/%n/merge_requests/new")
   (pullreq-refspec :initform "+refs/merge-requests/*/head:refs/pullreqs/*")))

(use-package github-review :ensure t)


;; colored pair of parenthesis
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))
;; smart parenthesis (and similar symbols)
(use-package smartparens
  :ensure t
  :init
  (require 'smartparens-config)
  :hook (prog-mode text-mode markdown-mode))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))
(use-package yasnippet-snippets
  :ensure t)

;; dash-docs: offline documentation for APIs
(use-package dash-docs :ensure t)

;; org-mode : notes/todos/…
(use-package org :ensure t)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/Documents/Algoo/inbox.org" "Tasks")
                               "* TODO %i%?")
                              ("T" "Tickler" entry
                               (file+headline "~/Documents/Algoo/tickler.org" "Tickler")
                               "* %i%? \n %U")))
(setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))


;; w3m
(use-package w3m :ensure t)

;; remove spaces/blanks from end of lines.
(use-package ws-butler
  :ensure t
  :config (ws-butler-global-mode 1))

;; plantuml
(use-package plantuml-mode :ensure t)
(setq plantuml-jar-path "~/.emacs.d/plantuml.jar")
(setq plantuml-default-exec-mode 'jar)

;; csv
(use-package csv-mode :ensure t)

;; multiple cursors
(use-package multiple-cursors :ensure t)
(global-set-key (kbd "C-c m c") 'mc/edit-lines)
(global-set-key (kbd "C-c m n") 'mc/mark-next-like-this)
(global-set-key (kbd "C-c m p") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c m a") 'mc/mark-all-like-this)
(global-set-key (kbd "C-c m d") 'mc/mark-all-dwim)

;; avy: better jump to text
(use-package avy
  :ensure t
  :config
  (global-set-key (kbd "C-f") 'avy-goto-char-timer))

;; .editorconfig support
(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

;; projectile to search files in project
;; useful to have ripgrep and fd (apt install ripgrep fd-find)
(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-k") 'projectile-command-map)
  (use-package projectile-ripgrep :ensure t))

;; enable integrated code-folding everywhere
(add-hook 'prog-mode-hook #'hs-minor-mode)

;; dark theme
;(use-package material-theme
;  :ensure t
;  :config
;  (load-theme 'material t)
					;)
(use-package nord-theme
  :ensure t
  :config
  (load-theme 'nord t))

(require 'todo-comment)
(defun set-todo-comment-keys ()
  (local-set-key (kbd "C-c i") 'insert-todo-comment))
(add-hook 'prog-mode-hook #'set-todo-comment-keys)

;; YAML support from https://raw.githubusercontent.com/yaml/yaml-mode/master/yaml-mode.el
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yaml$" . yaml-mode))

;; Docker packages
(use-package docker
  :ensure t
  :bind ("C-c d" . docker))
(use-package docker-compose-mode :ensure t)
(use-package dockerfile-mode :ensure t)
(require 'tramp-container)

;; Vagrant
(use-package vagrant-tramp :ensure t)

;; SQL formatting
(use-package sqlformat :ensure t)
(setq sqlformat-command 'sqlformat)

;; .bb (bitbake/yocto) files
(require 'bb-mode)
(add-to-list 'auto-mode-alist '("\\.bb$" . bb-mode))

;; PYTHON PACKAGES & CONFIGURATION
(setq exec-path (append exec-path '("~/.pyenv/bin")))
(setenv "WORKON_HOME" "~/.pyenv/versions/")
;; needs pip install python-lsp-server pyls-flake8 pyls-isort python-lsp-black mypy-ls
(use-package pyvenv
  :ensure t
  :init
  (setenv "WORKON_HOME" "~/.pyenv/versions")
  ;https://lists.gnu.org/archive/html/help-gnu-emacs/2021-09/msg00535.html
  (defun try/pyvenv-workon ()
    (when (buffer-file-name)
      (let* ((python-version ".python-version")
             (project-dir (locate-dominating-file (buffer-file-name) python-version)))
        (when project-dir
          (pyvenv-workon
            (with-temp-buffer
              (insert-file-contents (expand-file-name python-version project-dir))
              (car (split-string (buffer-string)))))))))

  :config
  (pyvenv-mode 1)
    (setq pyvenv-post-activate-hooks
        (list (lambda ()
                ;https://github.com/astoff/comint-mime#usage
                ;https://github.com/astoff/comint-mime/issues/2#issuecomment-922462074
                (when (executable-find "ipython3")
                  (setq python-shell-interpreter "ipython3"
                        ;python-shell-interpreter-args "--simple-prompt --classic"
                        ;https://elpy.readthedocs.io/en/latest/ide.html#interpreter-setup
                        python-shell-interpreter-args "-i --simple-prompt --classic --pprint")))))
  (setq pyvenv-post-deactivate-hooks
        (list (lambda ()
                (setq python-shell-interpreter "python3")))))
(use-package python-mode
  :hook (python-ts-mode . try/pyvenv-workon)
 :flymake-hook
 (python-mode
  flymake-collection-mypy                      ; Always added to diagnostic functions.
  (flymake-collection-ruff                   ; Added when predicate is true.
   :predicate (lambda ()
                (executable-find "ruff")))))

(use-package python-pytest :ensure t)
;; helper file to find the right pytest test root + key bindings
(defun python-pytest-config  ()
  (when-let ((r (locate-dominating-file default-directory ".pyroot")))
    (setq python-pytest-executable
          (concat "PYTHONPATH=" r " " "pytest")))
  (local-set-key (kbd "C-c t d") 'python-pytest-function-dwim)
  (local-set-key (kbd "C-c t f") 'python-pytest-file-dwim)
  (local-set-key (kbd "C-c t r") 'python-pytest-repeat)
  (local-set-key (kbd "C-c t a") 'python-pytest-all)
  (local-set-key (kbd "C-c t i") 'python-pytest-dispatch))
(add-hook 'python-ts-mode-hook #'python-pytest-config)

(use-package cython-mode :ensure t)


;; JAVASCRIPT PACKAGES & CONFIGURATION

; JS/JSX with js-mode + eglot
(add-to-list 'auto-mode-alist '("\\.js\\'" . js-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . js-mode))
(add-hook 'js-mode-ts-hook 'js2-minor-mode)

(with-eval-after-load 'js
  (define-key js-mode-map (kbd "M-.") nil))

; disable js2-mode errors as we rely on standard
(setq js2-mode-show-parse-errors nil)
(setq js2-mode-show-strict-warnings nil)

(use-package mocha :ensure t)
(defun mocha-config  ()
  (local-set-key (kbd "C-c t d") 'mocha-test-at-point)
  (local-set-key (kbd "C-c t f") 'mocha-test-file)
  (local-set-key (kbd "C-c t a") 'mocha-test-project))
(add-hook 'js-mode-hook #'mocha-config)

;; stylus-mode, manual install from https://github.com/vladh/stylus-mode
(require 'stylus-mode)
(add-to-list 'auto-mode-alist '("\\.styl$" . stylus-mode))

;; TYPESCRIPT
(use-package typescript-mode
  :ensure t
  :init
  (define-derived-mode typescript-tsx-mode typescript-mode "tsx")
  :config
  (setq typescript-indent-level 2)
  (add-hook 'typescript-ts-mode #'subword-mode)
  (add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescript-tsx-mode)))

;; GOLANG
(use-package go-mode :ensure t)

;; RUST
(use-package rust-mode :ensure t)
(use-package cargo-mode :ensure t)

;; ELM
(use-package elm-mode :ensure t)
(add-to-list 'auto-mode-alist '("\\.elm\\'" . elm-mode))
(add-hook 'elm-mode-hook #'disable-tabs)

;; Qt
(use-package qml-mode :ensure t)

;; Powershell
(use-package powershell :ensure t)

;; Music :-)
(use-package subsonic
  :ensure t
  :commands subsonic
  :bind (("C-c n" . subsonic))
  :custom
  (subsonic-url "music.amakaze.org/server")
  (subsonic-enable-art t))

;; Jinja2
(use-package jinja2-mode :ensure t)

;; Jenkinsfile
(use-package jenkinsfile-mode :ensure t)

;; CUSTOM
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   '("7922b14d8971cce37ddb5e487dbc18da5444c47f766178e5a4e72f90437c0711" "e6df46d5085fde0ad56a46ef69ebb388193080cc9819e2d6024c9c6e27388ba9" "7451f243a18b4b37cabfec57facc01bd1fe28b00e101e488c61e1eed913d9db9" default))
 '(global-display-line-numbers-mode t)
 '(js-chain-indent t)
 '(js-indent-level 2)
 '(js-switch-indent-offset 2)
 '(js2-bounce-indent-p t)
 '(package-selected-packages
   '(fira-code-mode material-theme zenburn-theme selectrum-prescient selectrum magit go-mode fd-dired ripgrep markdown-mode pyenv-mode xref-js2 mocha python-pytest dracula-theme yasnippet-snippets use-package find-file-in-project diminish))
 '(show-paren-mode t)
 '(subsonic-host "music.amakaze.org/server")
 '(todo-comment-author "SGD")
 '(todo-comment-types
   '((116 "TODO")
     (110 "NOTE")
     (105 "INFO")
     (102 "FIXME")
     (104 "HACK")))
 '(tool-bar-mode nil)
 '(typescript-indent-level 2)
 '(warning-suppress-log-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 113 :width normal)))))
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
